package helper

import (
	"fmt"

	"golang.org/x/net/context"

	firebase "firebase.google.com/go"

	"google.golang.org/api/option"
)

// NewFirebaeAdmin function
func NewFirebaeAdmin() (*firebase.App, error) {

	creds := []byte(`
	{
		"type": "service_account",
		"project_id": "binbin-store",
		"private_key_id": "d340556b0ad5ee337a60b31d2d10987aff9efad5",
		"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCUuwGGvJ6MXIJw\nV9dgKVJhvPUhM3hWmPaiFxzdeQZXYcl1Q1/b4XD6bWRUkhlkl3itv+EzOl2xqrea\nYtkJHEDnUfKpKKr2KClhEWv7iFXXH6IzErYoCH2+khBKTZjrFtfYQbiTKoWVpjaG\nf1mRprvniUjhxPVkBJotgRgjsMqcem/0yeTVB9Kzmni/oQYyRhz4DvkcO0MjvPRX\n3bez+1wvIYViQ6Np+Yw0kzaGjz+q9r9d0I26Wwc3c8MBJLZSS2NFfZnpgghOhmao\n6pwHAZPQIVfLZR3T2jmDrgx250LlPHMjTNf/i3YuhO1XunLZNzpmqiHawQU7TA4X\nAkgKhNKPAgMBAAECggEAAkfwY5vzLFKVHKjcOWrhta2WNbvsIFRRkj0NZWO1jr5p\naTj9TdFT+upczY5Z1q43lZftUrOWO9ThVG3vOU4Z2Ll6IbSwaMPVXNoxGuHMnzpx\no39UaaIZe5mgiziE/6YDJlRde6YZvtNdikS/gyQftvZ1KFvtm/kVNSQeYKhrBNrh\namCtQnpseIEQKTJviS3Mn6jXSz6RL1R1DfiwFysdyiC6N3XrFvCZ8lfpYnLQnDDr\njPiIDXZva0cU9U899zd/IsXV1AKJwzW0+KWhQp/E613QWDWOUDbc9MJQ+sXhCvR4\nRLqBorqd9DL1GK4NV/ezU+t6JB9/dS9mF5IV1CwDMQKBgQDRd1JrDLi/n8GuQNQt\n5ocjUN82YPRp9YOx+CmMyRGVT143qiuG7C/YJFNs72x3bkKHApwoF9Kv1U90VLeF\nKbyJ2B6cRNhhaB8kqP5ggOCBRyWthBn1QyLQoSuhSy4XwNHfXDzC8QsubBs/33S4\neA+N1t69QsXr9NJw3s1GhcQyAwKBgQC1xY0TcG8JuonHQm0ePOqbcHpXrhFdl9+B\n8HAegJhBfdO+MPThIE62iiTqU4/ObuoON6OjN33fS6hLSoPKQbZEGv9pKlBzzFU2\niHUrqv8PIgLtSVtE+u/wBxabOrrbWcz5sPYfm++NDk6h9ZMJgDBJcHWvhSslNrEn\nE19E3PmdhQKBgCQv0wM/bWNdfB5YgM91Nf955zYDxfXQPnIuhy8VRX5f+czwDxj+\nEL3seKUnL7vqRoAAiW27v+A0n8dfI11T+oht18vzcNZTIJ46u8d8PEzZN78kRT/D\n223jiiARVuMvopJN4wlijyXz8zkzp3x+8WD1sAFOWdcLspdhRmFalqx1AoGAYGgZ\njdDZI5jLEGLbyujWKYD7cluBHFm7kog6fwMHPd0Fm98NUgW3mbslHx8Yq41KGCZM\nZPm+idrvtJcd1P0LNY34HwfxSal7HIQ9WN7FjnokgUOh47EP5wdrZwM8LbCwNVI1\nB8jFwdNvwxijVqulcJ3z3RbNLlyWkroEklp9DZ0CgYBhzaExGgDbRQ3qkb4tbUqZ\nF7nGPrx1vPrazldyY8GbNaVFQCv/o56065ZZma8eMQBXt+Ihff84fErfg5DlLbQ1\nz4yy+ejJ864St4v2k2LkHUaAd69GBgV1MaZ60ht04fz4/5VbrRCue2MU4fsZZZRM\n30z/VbzTle8t4vz8EX6oNA==\n-----END PRIVATE KEY-----\n",
		"client_email": "firebase-adminsdk-1935u@binbin-store.iam.gserviceaccount.com",
		"client_id": "113270142038088651392",
		"auth_uri": "https://accounts.google.com/o/oauth2/auth",
		"token_uri": "https://oauth2.googleapis.com/token",
		"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
		"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-1935u%40binbin-store.iam.gserviceaccount.com"
	}
	
`)

	opt := option.WithCredentialsJSON(creds)
	app, err := firebase.NewApp(context.Background(), nil, opt)

	if err != nil {
		return nil, fmt.Errorf("error initializing app: %v", err)
	}

	return app, nil

}
