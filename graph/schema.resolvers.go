package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/mangbinbin/gateways/store-gateway/graph/generated"
	"gitlab.com/mangbinbin/gateways/store-gateway/graph/model"
	mw "gitlab.com/mangbinbin/gateways/store-gateway/middleware"
	pb "gitlab.com/mangbinbin/gateways/store-gateway/proxy"
)

func (r *couponResolver) Store(ctx context.Context, obj *model.Coupon) (*model.Store, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Store{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	rsp, err := s.GetStore(ctx, &pb.GetStoreRequest{
		StoreID: obj.StoreID,
	})

	if err != nil {
		return &model.Store{}, err
	}

	return &model.Store{
		ID:         rsp.Store.Id,
		Logo:       rsp.Store.Logo,
		Name:       rsp.Store.Name,
		Tagline:    rsp.Store.Tagline,
		Phone:      rsp.Store.Phone,
		Latitude:   rsp.Store.Latitude,
		Longitude:  rsp.Store.Longitude,
		Status:     rsp.Store.Status,
		CategoryID: rsp.Store.CategoryID,
	}, nil
}

func (r *couponResolver) Redeems(ctx context.Context, obj *model.Coupon) (int, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return 0, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	rsp, err := s.GetRedeems(ctx, &pb.GetRedeemsRequest{
		CouponID: obj.ID,
	})

	if err != nil {
		return 0, err
	}

	return int(rsp.Count), nil
}

func (r *couponResolver) Category(ctx context.Context, obj *model.Coupon) (*model.CouponCategory, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.CouponCategory{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	rsp, err := s.GetCouponCategory(ctx, &pb.GetCouponCategoryRequest{
		CategoryID: obj.CategoryID,
	})

	if err != nil {
		return &model.CouponCategory{}, err
	}

	return &model.CouponCategory{
		ID:   rsp.Category.Id,
		Name: rsp.Category.Name,
	}, nil
}

func (r *mutationResolver) SetTokenMessaging(ctx context.Context, input model.TokenMessagingInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewNotificationService()

	_, err := s.SetToken(ctx, &pb.SetTokenRequest{
		UserID: userID,
		Token:  input.Token,
		Type:   "store",
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) CreateStore(ctx context.Context, input model.NewStore) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	_, err := s.CreateStore(ctx, &pb.CreateStoreRequest{
		Logo:       input.Logo,
		Name:       input.Name,
		Tagline:    input.Tagline,
		Phone:      input.Phone,
		Latitude:   input.Latitude,
		Longitude:  input.Longitude,
		CategoryID: input.CategoryID,
		UserID:     userID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) UpdateStore(ctx context.Context, input model.UpdateStoreInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	_, err := s.UpdateStore(ctx, &pb.UpdateStoreRequest{
		StoreID:    input.StoreID,
		Logo:       input.Logo,
		Name:       input.Name,
		Tagline:    input.Tagline,
		Phone:      input.Phone,
		Latitude:   input.Latitude,
		Longitude:  input.Longitude,
		CategoryID: input.CategoryID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) CreateCoupon(ctx context.Context, input model.NewCoupon) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	_, err := s.CreateCoupon(ctx, &pb.CreateCouponRequest{
		Name:        input.Name,
		Photo:       input.Photo,
		Description: input.Description,
		Point:       input.Point,
		Condition:   input.Condition,
		CategoryID:  input.CategoryID,
		UserID:      userID,
		StoreID:     input.StoreID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) UpdateCoupon(ctx context.Context, input model.UpdateCouponInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	_, err := s.UpdateCoupon(ctx, &pb.UpdateCouponRequest{
		CouponID:    input.CouponID,
		Name:        input.Name,
		Photo:       input.Photo,
		Description: input.Description,
		Point:       input.Point,
		Condition:   input.Condition,
		CategoryID:  input.CategoryID,
		StoreID:     input.StoreID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) DeleteCoupon(ctx context.Context, input model.DeleteCouponInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	_, err := s.DeleteCoupon(ctx, &pb.DeleteCouponRequest{
		CouponID: input.CouponID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) RequestCouponBoost(ctx context.Context, input model.CouponBoostInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	_, err := s.CreateBoost(ctx, &pb.CreateBoostRequest{
		CouponIDs: input.CouponIDs,
		ProductID: input.ProductID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *queryResolver) GetStores(ctx context.Context, offset int, limit int) ([]*model.Store, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Store{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	var stores []*model.Store

	rsp, err := s.GetStoresByUser(ctx, &pb.GetStoresByUserRequest{
		UserID: userID,
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return stores, err
	}

	for _, store := range rsp.Stores {
		stores = append(stores, &model.Store{
			ID:         store.Id,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			Status:     store.Status,
			CategoryID: store.CategoryID,
		})
	}

	return stores, nil
}

func (r *queryResolver) GetStore(ctx context.Context, storeID string) (*model.Store, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Store{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	rsp, err := s.GetStore(ctx, &pb.GetStoreRequest{
		StoreID: storeID,
	})

	if err != nil {
		return &model.Store{}, err
	}

	return &model.Store{
		ID:         rsp.Store.Id,
		Logo:       rsp.Store.Logo,
		Name:       rsp.Store.Name,
		Tagline:    rsp.Store.Tagline,
		Phone:      rsp.Store.Phone,
		Latitude:   rsp.Store.Latitude,
		Longitude:  rsp.Store.Longitude,
		Status:     rsp.Store.Status,
		CategoryID: rsp.Store.CategoryID,
	}, nil
}

func (r *queryResolver) GetStoreCategories(ctx context.Context, offset int, limit int) ([]*model.StoreCategory, error) {
	s := r.NewStoreService()

	var categories []*model.StoreCategory

	rsp, err := s.GetStoreCategories(ctx, &pb.GetStoreCategoriesRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return categories, err
	}

	for _, category := range rsp.Categories {
		categories = append(categories, &model.StoreCategory{
			ID:   category.Id,
			Name: category.Name,
		})
	}

	return categories, nil
}

func (r *queryResolver) GetCoupons(ctx context.Context, storeID string, offset int, limit int) ([]*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var coupons []*model.Coupon

	rsp, err := s.GetCouponsByStore(ctx, &pb.GetCouponsByStoreRequest{
		StoreID: storeID,
		Offset:  int32(offset),
		Limit:   int32(limit),
	})

	if err != nil {
		return coupons, err
	}

	for _, coupon := range rsp.Coupons {

		coupons = append(coupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			CategoryID:  coupon.CategoryID,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
		})
	}

	return coupons, nil
}

func (r *queryResolver) GetApproveCoupons(ctx context.Context, storeID string, offset int, limit int) ([]*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var coupons []*model.Coupon

	rsp, err := s.GetApproveCouponByStore(ctx, &pb.GetApproveCouponByStoreRequest{
		StoreID: storeID,
		Offset:  int32(offset),
		Limit:   int32(limit),
	})

	if err != nil {
		return coupons, err
	}

	for _, coupon := range rsp.Coupons {

		coupons = append(coupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			CategoryID:  coupon.CategoryID,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
		})
	}

	return coupons, nil
}

func (r *queryResolver) GetCoupon(ctx context.Context, couponID string) (*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	rsp, err := s.GetCoupon(ctx, &pb.GetCouponRequest{
		CouponID: couponID,
	})

	if err != nil {
		return &model.Coupon{}, err
	}

	return &model.Coupon{
		ID:          rsp.Coupon.Id,
		Name:        rsp.Coupon.Name,
		Photo:       rsp.Coupon.Photo,
		Description: rsp.Coupon.Description,
		Point:       rsp.Coupon.Point,
		Condition:   rsp.Coupon.Condition,
		Expire:      rsp.Coupon.Expire,
		Status:      rsp.Coupon.Status,
		CategoryID:  rsp.Coupon.CategoryID,
		StoreID:     rsp.Coupon.StoreID,
	}, nil
}

func (r *queryResolver) GetCouponCategories(ctx context.Context, offset int, limit int) ([]*model.CouponCategory, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.CouponCategory{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var categories []*model.CouponCategory

	rsp, err := s.GetCouponCategories(ctx, &pb.GetCouponCategoriesRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return categories, err
	}

	for _, category := range rsp.Categories {
		categories = append(categories, &model.CouponCategory{
			ID:   category.Id,
			Name: category.Name,
		})
	}

	return categories, nil
}

func (r *queryResolver) GetCouponProducts(ctx context.Context, offset int, limit int) ([]*model.Product, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Product{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var products []*model.Product

	rsp, err := s.GetProducts(ctx, &pb.GetProductsRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return products, err
	}

	for _, product := range rsp.Products {
		products = append(products, &model.Product{
			ID:    product.Id,
			Name:  product.Name,
			Price: product.Price,
			Day:   int(product.Day),
		})
	}

	return products, nil
}

func (r *queryResolver) GetTransactions(ctx context.Context, storeID string, offset int, limit int) ([]*model.Transaction, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Transaction{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	var transactions []*model.Transaction

	rsp, err := s.GetStoreTransactions(ctx, &pb.GetStoreTransactionsRequest{
		StoreID: storeID,
		Offset:  int32(offset),
		Limit:   int32(limit),
	})

	if err != nil {
		return transactions, err
	}

	for _, transaction := range rsp.Transactions {

		transactions = append(transactions, &model.Transaction{
			ID:          transaction.Id,
			Description: transaction.Description,
			CreatedAt:   transaction.CreatedAt,
		})
	}

	return transactions, nil
}

func (r *queryResolver) GetTransaction(ctx context.Context, transactionID string) (*model.Transaction, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Transaction{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	rsp, err := s.GetStoreTransaction(ctx, &pb.GetStoreTransactionRequest{
		TransactionID: transactionID,
	})

	if err != nil {
		return &model.Transaction{}, err
	}

	return &model.Transaction{
		ID:          rsp.Transaction.Id,
		Description: rsp.Transaction.Description,
		CreatedAt:   rsp.Transaction.CreatedAt,
	}, nil
}

func (r *storeResolver) Category(ctx context.Context, obj *model.Store) (*model.StoreCategory, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.StoreCategory{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	rsp, err := s.GetStoreCategory(ctx, &pb.GetStoreCategoryRequest{
		CategoryID: obj.CategoryID,
	})

	if err != nil {
		return &model.StoreCategory{}, err
	}

	return &model.StoreCategory{
		ID:   rsp.Category.Id,
		Name: rsp.Category.Name,
	}, nil
}

// Coupon returns generated.CouponResolver implementation.
func (r *Resolver) Coupon() generated.CouponResolver { return &couponResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Store returns generated.StoreResolver implementation.
func (r *Resolver) Store() generated.StoreResolver { return &storeResolver{r} }

type couponResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type storeResolver struct{ *Resolver }
