package graph

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

import (
	"os"

	h "gitlab.com/mangbinbin/gateways/store-gateway/helper"
	pb "gitlab.com/mangbinbin/gateways/store-gateway/proxy"
)

// Resolver struct
type Resolver struct{}

// NewStoreService method
func (r *Resolver) NewStoreService() pb.StoreServiceClient {
	addr := os.Getenv("STORE_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewStoreServiceClient(conn)
}

// NewRedeemService method
func (r *Resolver) NewRedeemService() pb.RedeemServiceClient {
	addr := os.Getenv("REDEEM_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewRedeemServiceClient(conn)
}

// NewNotificationService method
func (r *Resolver) NewNotificationService() pb.NotificationServiceClient {
	addr := os.Getenv("NOTIFICATION_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewNotificationServiceClient(conn)
}
